using System;
using UnityEngine.Events;

namespace SINeDUSTRIES.QuickScope
{
  /// <summary>
  /// Runnable unit of a pipeline.
  /// ie, a pipeline or node.
  /// </summary>
  public interface IRun<TOutput>
  {
    #region methods

    // /// <summary>
    // /// Initialize the node, with inputs.
    // /// </summary>
    // /// <remarks>
    // /// It is useful to have Init before Start.
    // /// </remarks>
    // void Init(TInput input);

    /// <summary>
    /// Continue running.
    /// Finishes itself.
    /// </summary>
    /// <param name="output">default if not.</param>
    /// <returns>true: finished? false: did not finish.</returns>
    Boolean Run(out TOutput output);

    /// <summary>
    /// Start running.
    /// Invoked 1 cycle (ie, Update) after <see cref="Awake"/>.
    /// </summary>
    void Start();

    #endregion

    #region Properties

    /// <summary>
    /// Current state of this runnable.
    /// </summary>
    RunState State { get; }

    #endregion
  }
}