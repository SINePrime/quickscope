using System;
using System.Collections.Generic;

namespace SINeDUSTRIES.QuickScope.Nodes
{
  /// <summary>
  /// Output of <see cref="Nodes.Write"/>.
  /// </summary>
  public struct Write_Output
  {
    /// <summary>
    /// Paths to the files.
    /// </summary>
    public IEnumerable<String> Paths { get; }

    public Write_Output(IEnumerable<String> paths)
    {
      this.Paths = paths;
    }
  }
}