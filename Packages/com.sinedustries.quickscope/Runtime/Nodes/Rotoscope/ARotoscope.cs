using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Events;
using UnityEngine.Playables;

using UnityEditor;

using SINeDUSTRIES.Unity;

namespace SINeDUSTRIES.QuickScope.Nodes
{
  /// <summary>
  /// Node in the pipeline which does the rotoscoping.
  /// ie, generates <see cref="Sample"/> for graphics, and optionally, depth.
  /// </summary>
  /// <remarks>
  /// Implicitly a Singleton, as it reads from a static class.
  /// Multiple instances have unexpcted behaviours.
  /// </remarks>
  public abstract class ARotoscope : ARun<Rotoscope_Output>
  {
    #region this / methods / protected

    protected void rotoscope(String clipName = "STILL", Int32 clipSampleIndex = 0)
    {
      // setup angles
      Single capture_angleRad = this.configView.AngleYStart * Mathf.Deg2Rad;
      Single capture_angleRadIncrement = (2 * Mathf.PI) / this.configView.AngleYCuts; // 2pi radians in circle, cut into equal pieces

      // iterate over angles
      for (Int32 iViewAngleIndex = 0; iViewAngleIndex < this.configView.AngleYCuts; iViewAngleIndex++)
      {
        // position the target
        this.cameraRotoscope.transform.LookAt(
          this.target_Clone.transform,
          this.configView.DistanceXZ,
          this.configView.HeightY,
          capture_angleRad,
          this.configTarget.ObjectHeight
        );

        // clear the render texture to avoid stacking
        this.cameraRotoscope_RenderTexture.Release();

        // create sprite
        Texture2D textureSprite = this.cameraRotoscope.Screenshot(this.cameraRotoscope_RenderTexture); // invokes Camera.Render

        // create normals
        RenderTexture depthNormalsRenderTexture = (RenderTexture)Shader.GetGlobalTexture("_CameraDepthNormalsTexture");
        Texture2D depthNormals = Texture2DUtils.ToTexture2D(depthNormalsRenderTexture);
        Texture2D textureNormals = depthNormals.NormalsDecodeDepthNormals();

        // TODO: make this enumerable, yield return

        this.outputs.Add(
          new Sample(
            this.configTarget.name,
            clipName,
            clipSampleIndex,
            this.configView.name,
            iViewAngleIndex,
            textureSprite,
            textureNormals
          )
        );

        capture_angleRad += capture_angleRadIncrement;
      }
    }

    #endregion

    #region ARun / methods / protected

    override protected /* concrete */ void start()
    {

    }

    override protected /* concrete */ void finish()
    {
      // cleanup cameraRotoscope
      if (this.cameraRotoscope_IsTemp)
      {
        UnityEngine.Object.DestroyImmediate(this.cameraRotoscope);
        UnityEngine.Object.DestroyImmediate(this.cameraRotoscope_GameObject);
      }

      // cleanup target
      UnityEngine.Object.DestroyImmediate(this.target_Clone);

      // restore cameraMain
      if (this.cameraMain)
      {
        this.cameraMain.depthTextureMode = cameraMain_DepthTextureModeMemento;
      }

      Debug.LogFormat($"Pipeline / Rotoscope: Finished.");
    }

    #endregion

    #region fields / animation

    /// <summary>
    /// Animations are played on this.
    /// </summary>
    protected GameObject target_Clone;

    #endregion

    #region fields / camera

    /// <summary>
    /// Camera to rotoscope from.
    /// </summary>
    private Camera cameraRotoscope;

    /// <summary>
    /// Is <see cref="cameraGameObject"/> temporary?
    /// </summary>
    private bool cameraRotoscope_IsTemp;

    /// <summary>
    /// GameObject the camera is on.
    /// </summary>
    private GameObject cameraRotoscope_GameObject;

    /// <summary>
    /// <see cref="RenderTexture"/> to create sprites with.
    /// </summary>
    private RenderTexture cameraRotoscope_RenderTexture;

    #endregion

    #region fields / main camera

    private Camera cameraMain;

    private DepthTextureMode cameraMain_DepthTextureModeMemento;

    #endregion

    #region fields / entrypoints

    protected IList<Sample> outputs { get; private set; } = new List<Sample>();

    #endregion

    #region fields / config

    protected IConfigTarget configTarget;

    protected IConfigView configView;

    protected IConfigTextures configTextures;

    #endregion

    #region constructors

    public ARotoscope(IConfigTarget configTarget, IConfigView configView, IConfigTextures configTextures)
    {
      this.configTarget = configTarget;
      this.configView = configView;
      this.configTextures = configTextures;

      // animations - setup animation root
      this.target_Clone = GameObject.Instantiate(this.configTarget.Object);

      // animations - get animator
      Animator animator = this.target_Clone.GetComponentInChildren<Animator>();
      if (animator == null)
      {
        throw new UnityEngine.MissingComponentException($"Pipeline / Rotoscope: Target {this.configTarget.Object} is missing {nameof(Animator)} Component.");
      }

      // cameraMain
      this.cameraMain = Camera.main;
      if (this.cameraMain != null)
      {
        this.cameraMain_DepthTextureModeMemento = this.cameraMain.depthTextureMode;
        this.cameraMain.depthTextureMode = DepthTextureMode.None; // main camera must be DepthTexture.None, so Scene camera won't capture
      }

      // cameraRotoscope
      this.cameraRotoscope_IsTemp = true;
      this.cameraRotoscope_GameObject = new GameObject(".QuickScope_Camera");
      this.cameraRotoscope = this.cameraRotoscope_GameObject.AddComponent<Camera>();
      this.cameraRotoscope.depthTextureMode = DepthTextureMode.DepthNormals;
      this.cameraRotoscope.clearFlags = CameraClearFlags.Depth;

      Vector3Int screenDimensions = new Vector3Int(0, 0, (Int32)(this.configView.DistanceXZ * 2f));

      // determine screen resolution for RenderTexture
#if UNITY_EDITOR
      Vector2 gameView = UnityEditor.Handles.GetMainGameViewSize();
      screenDimensions.x = (Int32)gameView.x;
      screenDimensions.y = (Int32)gameView.y;
#else
      screenDimensions.x = Screen.currentResolution.x;
      screenDimensions.y = Screen.currentResolution.y;
#endif
      this.cameraRotoscope_RenderTexture = new RenderTexture(screenDimensions.x, screenDimensions.y, screenDimensions.z);
      if (this.configTextures.AntiAliasing != 0)
      {
        this.cameraRotoscope_RenderTexture.antiAliasing = this.configTextures.AntiAliasing;
      }
      this.cameraRotoscope.targetTexture = this.cameraRotoscope_RenderTexture;
    }

    #endregion
  }
}