namespace SINeDUSTRIES.QuickScope.Nodes
{
  public class RotoscopeStill : ARotoscope
  {
    #region ARun / methods

    protected override bool run(out Rotoscope_Output output)
    {
      this.rotoscope();

      output = new Rotoscope_Output(base.outputs);

      return true;
    }

    #endregion

    #region constructors

    public RotoscopeStill(IConfigTarget configTarget, IConfigView configView, IConfigTextures configTextures) : base(configTarget, configView, configTextures)
    {

    }

    #endregion
  }
}