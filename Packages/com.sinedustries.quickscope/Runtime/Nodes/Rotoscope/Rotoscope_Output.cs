using System.Collections.Generic;

namespace SINeDUSTRIES.QuickScope.Nodes
{
  /// <summary>
  /// Output for <see cref="ARotoscope"/>.
  /// </summary>
  public class Rotoscope_Output
  {
    public IEnumerable<Sample> Rotoscopes { get; private set; }

    public Rotoscope_Output(IEnumerable<Sample> rotoscopes)
    {
      this.Rotoscopes = rotoscopes;
    }
  }
}