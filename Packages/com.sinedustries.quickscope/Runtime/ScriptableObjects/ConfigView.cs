using System;

using UnityEngine;

namespace SINeDUSTRIES.QuickScope
{
  /// <summary>
  /// Describes the transform of the Camera while rotoscoping.
  /// </summary>
  [CreateAssetMenu(fileName = nameof(ConfigView), menuName = "QuickScope/" + nameof(ConfigView))]
  public class ConfigView : ScriptableObject,
    IConfigView
  {
    #region properties

    /// <summary>
    /// <see cref="IConfigView.DistanceXZ"/>.  
    /// </summary>
    public Single DistanceXZ =>
      this.distanceXZ;

    /// <summary>
    /// <see cref="IConfigView.Height"/>.  
    /// </summary>
    public Single HeightY =>
      this.heightY;

    /// <summary>
    /// <see cref="IConfigView.AngleYStart"/>.  
    /// </summary>
    public Single AngleYStart =>
      this.angleYStart;

    /// <summary>
    /// <see cref="IConfigView.AngleYCuts"/>.  
    /// </summary>
    public Int32 AngleYCuts =>
      this.angleYCuts;

    #endregion

    #region fields

    [SerializeField]
    private Single distanceXZ;

    [SerializeField]
    private Single heightY;

    [SerializeField]
    private Single angleYStart;

    [SerializeField]
    private Int32 angleYCuts;

    #endregion
  }
}