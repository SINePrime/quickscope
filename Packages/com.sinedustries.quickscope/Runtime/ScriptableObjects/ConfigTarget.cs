using System;

using UnityEngine;

using SINeDUSTRIES.Unity;

namespace SINeDUSTRIES.QuickScope
{
  /// <summary>
  /// Configuration
  /// </summary>
  [CreateAssetMenu(fileName = nameof(ConfigTarget), menuName = "QuickScope/" + nameof(ConfigTarget))]
  public class ConfigTarget : ScriptableObject,
    IConfigTarget
  {
    #region config

    /// <summary>
    /// Target of the camera.
    /// Searches for <see cref="Animator"/> in children.
    /// </summary>
    public GameObject Object =>
      this.target;

    /// <summary>
    /// Height of the target.
    /// </summary>
    public Single ObjectHeight =>
      this.targetHeight;

    /// <summary>
    /// Clips to apply.
    /// </summary>
    public AnimationClipArray Clips =>
      this.clips;

    #endregion

    #region fields

    [SerializeField]
    private GameObject target;

    [SerializeField]
    private AnimationClipArray clips;

    [SerializeField]
    private Single targetHeight;

    #endregion
  }
}