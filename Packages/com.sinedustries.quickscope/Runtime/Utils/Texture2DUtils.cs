using System;

using UnityEngine;

namespace SINeDUSTRIES.Unity
{
  /// <summary>
  /// Utility methods for <see cref="Texture2D"/>.
  /// </summary>
  static public class Texture2DUtils
  {
    /// <summary>
    /// Create a new <see cref="Texture2D"/> from the currently active <see cref="RenderTexture"/> or the view.
    /// </summary>
    static public Texture2D New
      (
        Vector2Int sourcePosition,
        Vector2Int sourceDimensions,
        Vector2Int targetPosition,
        Vector2Int targetDimensions,
        TextureFormat format = TextureFormat.ARGB32,
        Boolean mipChain = true,
        Boolean linear = false
      )
    {
      Rect source = new Rect(
        sourcePosition.x, // x start
        sourcePosition.y, // y start
        sourcePosition.x + sourceDimensions.x, // x finish
        sourcePosition.y + sourceDimensions.y // y finish
      );

      Texture2D texture = new Texture2D(targetDimensions.x, targetDimensions.y, format, mipChain, linear);
      texture.ReadPixels(source, targetPosition.x, targetPosition.y, true); // read and apply

      return texture;
    }

    /// <summary>
    /// Create a new <see cref="Texture2D"/> from the currently active <see cref="RenderTexture"/> or the view.
    /// </summary>
    static public Texture2D New(Vector2Int dimensions, TextureFormat format = TextureFormat.ARGB32, Boolean mipChain = true, Boolean linear = false)
    {
      return New(
        new Vector2Int(0, 0),
        dimensions,
        new Vector2Int(0, 0),
        dimensions,
        format,
        mipChain,
        linear
      );
    }

    /// <summary>
    /// Create a new <see cref="Texture2D"/> from a <see cref="RenderTexture"/>.
    /// </summary>
    static public Texture2D ToTexture2D
      (
        this RenderTexture thisRenderTexture,
        TextureFormat format = TextureFormat.ARGB32,
        Boolean mipChain = true,
        Boolean linear = false
      )
    {
      RenderTexture mementoActive = RenderTexture.active; // store memento
      RenderTexture.active = thisRenderTexture; // set active, so ReadPixels

      // new Texture2D via ReadPixels
      Texture2D texture = Texture2DUtils.New(
        new Vector2Int(thisRenderTexture.width, thisRenderTexture.height),
        format,
        mipChain,
        linear
      );

      // restore Active
      RenderTexture.active = mementoActive;

      return texture;
    }

    /// <summary>
    /// Writes a <see cref="Texture2D"/> to abstract file.
    /// </summary>
    static public void WriteToPNG(this Texture2D thisTexture2D, String path)
    {
      Byte[] bytes = thisTexture2D.EncodeToPNG();

      System.IO.File.WriteAllBytes(path, bytes);
    }
  }
}