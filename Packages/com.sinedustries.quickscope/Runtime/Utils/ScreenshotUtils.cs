using System;

using UnityEngine;

namespace SINeDUSTRIES.Unity
{
  static public class CameraUtils
  {
    /// <summary>
    /// LookAt with extra parameters.
    /// </summary>
    /// <param name="targetHeight">Height of the <paramref name="target"/>. Camera position is adjusted by half of this, after looking at the target. Does not affect angle.</param>
    /// <param name="angleYRad">Angle, in radians, to rotate about the global Y axis.</param>
    static public void LookAt(this Transform thisTransform, Transform target, Single distanceXZ, Single heightY, Single angleYRad, Single targetHeight = 0)
    {
      Vector3 position = target.transform.position; // start at target
      //Debug.Log($"Target {position}");

      position.y += heightY; // add height

      Boolean topDown = Mathf.Approximately(distanceXZ, 0); // XZ distance not zero

      if (topDown)
      {
        thisTransform.transform.localEulerAngles = new Vector3(90, 0, 0); // look straight down
        thisTransform.transform.RotateAround(thisTransform.transform.position, Vector3.up, angleYRad * Mathf.Rad2Deg);
      }
      else
      {
        // if not topdown, then add distance

        Vector2 xzOffset = new Vector2(Mathf.Cos(angleYRad), Mathf.Sin(angleYRad)) * distanceXZ; // return a new vector

        position.x += xzOffset.x;
        position.z += xzOffset.y;

        // calculate angle before adjusting for target height
        // ie, all shots will have same angle, regardless of target height
        thisTransform.transform.position = position;
        thisTransform.transform.LookAt(target.transform); // look at the target, calculating the angle
      }

      // apply height offset
      position.y += targetHeight / 2; // adjust for half of target's height after calculating angle, but before taking shot

      // apply position
      thisTransform.transform.position = position;
    }

    /// <summary>
    /// Create a new <see cref="Texture2D"/> using a <see cref="Camera"/> and a <see cref="RenderTexture"/>.
    /// </summary>
    /// <remarks>
    /// Invokes <see cref="Camera.Render"/>.
    /// <paramref name="renderTexture"/> is an argument to provide more control. 
    /// </remarks>
    static public Texture2D Screenshot
      (
        this Camera thisCamera,
        RenderTexture renderTexture,
        TextureFormat format = TextureFormat.ARGB32,
        Boolean mipChain = true,
        Boolean linear = false
      )
    {
      // memento - cache
      RenderTexture mementoCamera = thisCamera.targetTexture; // store memento of camera

      // set target RenderTexture, so that camera will render to this RenderTexture
      thisCamera.targetTexture = renderTexture;

      // render to RenderTexture
      thisCamera.Render();

      // create texture from the RenderTexture, which was rendered to
      Texture2D texture = Texture2DUtils.ToTexture2D(renderTexture);

      // memento - restore
      thisCamera.targetTexture = mementoCamera;

      return texture;
    }

    /// <summary>
    /// Take a screenshot using a camera.
    /// A new <see cref="RenderTexture"/> is created.
    /// </summary>
    /// <remarks>
    /// Invokes <see cref="Camera.Render"/>.
    /// </remarks>
    /// <param name="dimensions">Used to create <see cref="RenderTexture"/> for screenshot. z is "depth".</param>
    static public Texture2D Screenshot(this Camera thisCamera, Vector3Int dimensions) =>
      thisCamera.Screenshot(new RenderTexture(dimensions.x, dimensions.y, dimensions.z));
  }
}