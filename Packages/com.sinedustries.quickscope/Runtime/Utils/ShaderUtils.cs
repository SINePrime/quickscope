using System;

using UnityEngine;

namespace SINeDUSTRIES.Unity
{
  static public class ShaderUtils
  {
    /// <summary>
    /// Encode a normal direction into a color, for use as a pixel in a normal texture.
    /// </summary>
    static public Color NormalEncode(this Vector3 normal) =>
      new Color((normal.x + 1) / 2, (normal.y + 1) / 2, (normal.z + 1) / 2, 1);

    /// <summary>
    /// Decode a DepthNormal to a Normal.
    /// ie, decode DepthNormal pixel into a Normal direction.
    /// </summary>
    /// <remarks>Ported from DecodeViewNormalStereo in UnityCG.cginc.</remarks> 
    /// <return>
    /// 3D direction vector that describes the normal. ie, NOT encoded as a color, NOT suitable for writing to normal map.
    /// </return>
    static public Vector3 NormalDecodeDepthNormal(this Vector4 depthNormalEncoded)
    {
      Single kScale = 1.7777f;
      Vector3 nn = Vector3.Scale((Vector3)depthNormalEncoded, new Vector3(2 * kScale, 2 * kScale, 0)) + new Vector3(-kScale, -kScale, 1);
      float g = 2.0f / Vector3.Dot(nn, nn);

      Vector3 n;
      n = g * (Vector2)nn;
      n.z = g - 1;

      return n;
    }

    /// <summary>
    /// Decode a Float from a Vector2.
    /// </summary>
    /// <remarks>Ported from DecodeFloatRG in UnityCG.cginc.</remarks>
    static public Single FloatDecodeVector2(this Vector2 floatEncoded)
    {
      Vector2 kDecodeDot = new Vector2(1.0f, 1f / 255.0f);
      return Vector2.Dot(floatEncoded, kDecodeDot);
    }

    /// <summary>
    /// Decode a DepthNormals texture to a Normals texture.
    /// </summary>
    static public Texture2D NormalsDecodeDepthNormals(this Texture2D depthNormalsEncoded)
    {
      Color[] sourcePixels = depthNormalsEncoded.GetPixels();
      Color[] normals = new Color[sourcePixels.Length];

      // convert DepthNormals -> normals
      for (Int32 i = 0; i < sourcePixels.Length; i++)
      {
        Color iDepthNormal = sourcePixels[i];

        // get the normal direction, then encode the direction as 
        normals[i] = NormalDecodeDepthNormal(iDepthNormal).NormalEncode(); // DepthNormal<Color> to Normal<Vector3> -> NormalColor<Color>
      }

      // create target
      Texture2D target = new Texture2D(depthNormalsEncoded.width, depthNormalsEncoded.height);
      target.SetPixels(normals);
      target.Apply();

      return target;
    }
  }
}